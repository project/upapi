<?php

/**
 * @file 
 * The UpAPI (short for Upload API) module provides a streamlined
 * interface for handling files uploaded to the website.
 * 
 * The goal is to free module developers from the confusing mess that is
 * file.inc and make it a shit-ton easier to create custom content types
 * with one or more files associated.
 * 
 * The API can be used to associate files with any object.  Not just nodes.
 */

// Things files can be associated with:
define('UPAPI_TYPE_NODE_ID', 'node');
define('UPAPI_TYPE_NODE_REVISION', 'revision'); // not yet supported
define('UPAPI_TYPE_USER_ID', 'user'); // not yet supported

// Operations for hook
define('UPAPI_OP_UPLOAD', 'upload');
define('UPAPI_OP_PREVIEW_PREPARE', 'preview prepare');
define('UPAPI_OP_INSERT', 'insert');
define('UPAPI_OP_LOAD', 'load');
define('UPAPI_OP_DOWNLOAD', 'download');
define('UPAPI_OP_MAY_DELETE', 'may_delete');
define('UPAPI_OP_MAY_DOWNLOAD', 'may_download');
define('UPAPI_OP_DELETED', 'deleted');
define('UPAPI_OP_UPDATED', 'updated');

/**
 * Implementation of hook_menu.
 */
function upapi_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    // Our download callback for files not visible directly by a URL.
    $items[] = array('path' => 'upapi/download',
                     'callback' => '_upapi_download',
                     'access' => TRUE,
                     'type' => MENU_CALLBACK);
  }
  return $items;
}

/**
 * Implementation of hook_nodeapi.
 * 
 * Here we make a note of brand new nodes.  This helps us assign files to
 * newly created nodes.
 */
function upapi_nodeapi(&$node, $op, $a3) {
  if ($op == 'insert') {
    // Remember the ID of the newly created node, for future reference
    _upapi_id_helper(UPAPI_TYPE_NODE_ID, $node->nid);
  }
  // TODO: delete files automatically when node deleted.
}

/**
 * This is our implementation of the upfield callback.  The upfield form
 * elements will call us here to notify us of file uploads, deletes and
 * changes.
 * 
 * UpAPI handles the basic bookkeeping of saving the file to the proper
 * directory and keeping track of it in the database.
 * 
 * Other modules (i.e. yours) will be able to act on file events via
 * hook_upapi.
 */
function upapi_callback($label, $data, &$file, $op, $detail) {
  // We use label in more places than upfield does.
  $data->label = $label;

  //drupal_set_message("upapi_callback($op)" . dpr($data, 1) . dpr($file, 1) . dpr($detail, 1)); // debug

  if ($op == UPFIELD_OP_NEW_UPLOAD) {
    // First time we've seen this file.  Fill in defaults.
    $file->description = $file->filename;
    $file->weight = 0;
    
    // Store our data with file.  Used in upapi_query().
    foreach (array('obj_type', 'obj_id', 'label') as $key) {
      $file->$key = $data->$key;
    }

    _upapi_invoke($file, UPAPI_OP_UPLOAD, $data);
  }
  else if ($op == UPFIELD_OP_PREPARE) {
    // File has been uploaded already.  Details may have been modified during
    // preview or submit.
    if (count($detail)) // Details from form POST
      foreach(array('description', 'delete', 'weight') as $key)
        $file->$key = $detail[$key];

    if (!$file->delete) // Skip callback if file will be deleted.  Right?
      _upapi_invoke($file, UPAPI_OP_PREVIEW_PREPARE, $data);
  }
  else if ($op == UPFIELD_OP_SUBMIT) {
    if ($file->upapi_id && $file->delete) {
      // Handle deletes.
      upapi_delete($file);
    }
    else if ($file->upapi_id) {
      // Handle updates.
      upapi_update($file);
    }
    else {
      // Handle newly uploaded files.
      if (!$file->delete) {
        // Where to save file?
        $dest = _upapi_get_destination($data, $file);
        _upapi_mkdir($dest);
        
        // Save new file to permanent location
        if ($saved = file_save_upload($file, $dest)) {
          $log = t("Saved upload %source (%name) to %dest.",
                   array('%source' => $file->filepath,
                         '%dest' => $dest,
                         '%name' => $file->filename));
          watchdog('upapi', $log);
          // Update database
          upapi_insert($data, $saved);
        }
        else {
          $log = t("Failed to save upload %source (%name) to %dest.",
                               array('%source' => $file->filepath,
                                     '%dest' => $dest,
                                     '%name' => $file->filename));
          drupal_set_message($log, 'error');
          watchdog('upapi', $log, 'error');
        }
      }
      else {
        // An uploaded file was deleted without ever being permanently saved.
        // Nothing to do.
      }
    }
  }
  else if ($op == UPFIELD_OP_GET_FILES) {
    // Form field is asking us for a list of files to include as default.
    $files = _upapi_query($data, FALSE);
    return $files;
  }
}

/**
 * Save new file data to the database.
 */
function upapi_insert($data, &$file) {
  global $user;
  // TODO: some checking to make sure values passed in are OK.
  
  $fid = db_next_id('{upapi_file}_fid'); // best way?
  $file->fid = $fid;
  db_query("INSERT INTO {upapi_file} (fid, uid_upload, filename, filepath, filemime, filesize) VALUES (%d, %d, '%s', '%s', '%s', %d)",
           $fid, $user->uid, 
           $file->filename,
           $file->filepath,
           $file->filemime,
           $file->filesize);
  
  // If node or other object is newly created, make sure we know its ID
  if (!$data->obj_id)
    $data->obj_id = _upapi_id_helper($data->obj_type);
  db_query("INSERT INTO {upapi_data} (fid, obj_id, obj_type, label, description, uid_data, weight) VALUES (%d, %d, '%s', '%s', '%s', %d, %d)",
           $fid, $data->obj_id, $data->obj_type,
           $data->label, $file->description, $user->uid, $file->weight);

  // Populate some fields before invoking callback.
  // Especially important if obj_id was just learned.
  $file->label = $data->label;
  $file->obj_id = $data->obj_id;
  $file->obj_type = $data->obj_type;
  
  _upapi_invoke($file, UPAPI_OP_INSERT);
}

/*
 * Delete a file from both the database and file system.
 * 
 * We call hook_upapi with $op == UPAPI_OP_MAY_DELETE before deletion.  A
 * third-pary module may prevent deletion by setting $a3 to FALSE.
 */
function upapi_delete($file) {
  $deleted = FALSE;

  // Determine whether others refer to this file.
  $refs = _upapi_query(array('upapi_id' => $file->upapi_id), FALSE);
  $ref = $refs[0];

  // Delete our reference.
  db_query("DELETE FROM {upapi_data} WHERE upapi_id = %d",
           $file->upapi_id);

  if (count($refs) == 1 && $ref->upapi_id == $file->upapi_id) {
    // We are deleting the only reference to the file, it is safe to delete.
    // But first we ask other modules.
    $may_delete = _upapi_invoke($file, UPAPI_OP_MAY_DELETE, TRUE);
    if ($may_delete) {
      db_query("DELETE FROM {upapi_file} WHERE fid = %d",
               $file->fid);
      file_delete($file->filepath);
      _upapi_invoke($file, UPAPI_OP_DELETED);
      $deleted = TRUE;
      //TODO: watchdog log message
    }
  }
  return $deleted;
}

/**
 * Updates the information about a file in the database.
 */
function upapi_update($file) {
  $refs = _upapi_query(array('upapi_id' => $file->upapi_id), FALSE);
  $ref = $refs[0];
  
  db_query("UPDATE {upapi_data} SET description='%s', weight='%s', fid=%d, obj_id=%d, obj_type='%s', label='%s' WHERE upapi_id=%d",
           $file->description, 
           $file->weight,
           $file->fid,
           $file->obj_id,
           $file->obj_type,
           $file->label,
           $file->upapi_id);
  _upapi_invoke($file, UPAPI_OP_UPDATED, $ref);
}

/**
 * Loads data structures representing the files.
 * 
 * 
 * @param $data 
 * An associative array limiting the files to load.  If an
 * upapi_id is known, $data could be ('upapi_id' => NNN).  If you want to load
 * all files associated with a node, try ('obj_type' => UPAPI_TYPE_NODE_ID,
 * 'obj_id' => NNN).  There are many possibilities.
 * 
 * @return 
 * An array of file objects.  It's always an array, even if just one
 * file is found.  And it's worth noting that the returned values take into
 * account files being 'previewed' during the node (or other object) update
 * process.  So using this to get the files associated with your object should
 * work, even in the middle of an edit process.
 */
function upapi_load($data) {
  $items = array();
  $results = _upapi_query($data);
  foreach ($results as $file) {
    _upapi_invoke($file, UPAPI_OP_LOAD);
    $items[] = $file;
  }
  return $items;
}

/*
 * Internal use only.  The magic behind upapi_load.
 */
function _upapi_query($data, $preview = TRUE) {
  $data = (array) $data;
  $items = array();

  // Build database query
  foreach (array('obj_type', 'label', 'description') as $param) {
    if (isset($data[$param])) {
      $cond[] = "uad.$param = '%s'";
      $arg[] = $data[$param];
    }
  }
  foreach (array('upapi_id', 'obj_id', 'uid_data', 'weight') as $param) {
    if (isset($data[$param])) {
      $cond[] = "uad.$param = %d";
      $arg[] = $data[$param];
    }
  }
  
  foreach (array('filepath', 'filemime') as $param) {
    if (isset($data[$param])) {
      $cond[] = "uaf.$param = '%s'";
      $arg[] = $data[$param];
    }
  }
  foreach (array('fid', 'uid_upload', 'filesize') as $param) {
    if (isset($data[$param])) {
      $cond[] = "uaf.$param = %d";
      $arg[] = $data[$param];
    }
  }
  
  $cond = implode(' AND ', $cond);
  $result = db_query("SELECT uaf.*, uad.* FROM {upapi_file} uaf LEFT JOIN {upapi_data} uad ON uad.fid=uaf.fid WHERE $cond ORDER BY uad.label,uad.weight", $arg);

  $i = 0;
  $map = array();
  while ($file = db_fetch_object($result)) {
    $items[$i] = $file;
    $map[$file->upapi_id] = $i;
    $i++;
  }
  
  if ($preview) {
    // Include latest uploads, in case we are previewing
    $files = upfield_editable_files();
    
    if (count($files)) {
      foreach ($files as $file) {
        if ($file->upapi_id && isset($map[$file->upapi_id])) {
          // File is not a new upload, but is being edited.
          // Replace the details from the database with details from the form.
          if ($file->delete || $file->replace)
            unset($items[$map[$file->upapi_id]]);
          else
            $items[$map[$file->upapi_id]] = $file;
        }
        else {
          // File is new upload.  Not yet submitted.
          if (!$file->delete)
            if ((!$data->obj_type ||
                 $file->obj_type == $data->obj_type) &&
                (!$data->obj_id ||
                 $file->obj_id == $data->obj_id) &&
                (!$data->label ||
                 $file->label == $data->label))
              $items[] = $file;
        }
      }
      // TODO: order recent uploads, by weight and possibly other
    }
  }

  return $items;

}

function _upapi_get_destination($data, $file) {
  global $user;
  if ($data->destination) {
    $obj_id = $obj_id;
    if (!$obj_id)
      $obj_id = _upapi_id_helper($data->obj_type);
    $dest = strtr($data->destination,
                  array('%filename' => $file->filename,
                        '%uid' => $user->uid,
                        '%obj_id' => $obj_id,
                        '%obj_type' => $data->obj_type,
                        '%label' => $data->label,
                        '%file_directory' => file_directory_path(),
                  ));
    return $dest;
  }  
}

// from http://us2.php.net/mkdir
/**
 * Attempt to make the directories required to save a file.
 */
function _upapi_mkdir($path, $rights = 0775) {
  $folder_path = array(strstr($path, '.') ? dirname($path) : $path);
  
  while(!@is_dir(dirname(end($folder_path)))
        && dirname(end($folder_path)) != '/'
        && dirname(end($folder_path)) != '.'
        && dirname(end($folder_path)) != '')
    array_push($folder_path, dirname(end($folder_path)));
  
  while($parent_folder_path = array_pop($folder_path))
    if (!file_exists($parent_folder_path))
      if(!@mkdir($parent_folder_path, $rights))
        drupal_set_message(t("Failed to create folder %path.",
                             array('%path' => $parent_folder_path)));
}

/**
 * Returns the URL path which can download the file.
 */
function upapi_download_path($file) {
  if ($file->upapi_id) {
    //File has been saved.
    return 'upapi/download/'.$file->upapi_id;
  }
  else {
    // File has been uploaded and may be previewed
    return upfield_preview_path($file);
  }
}

/**
 * Our menu callback which downloads files.
 * 
 * Calls hook_upapi to ask whether the download is allowed.  If no module sets
 * $a3 to TRUE, access will be denied.  Modules should only change $a3 if they
 * are the module responsible for the file.
 */
function _upapi_download($id) {
  $files = _upapi_query(array('upapi_id' => $id), FALSE);
  if (count($files)) {
    $file = $files[0];
    // Assume download not allowed, unless some module allows it.
    $may_download = _upapi_invoke($file, UPAPI_OP_MAY_DOWNLOAD, FALSE);
    if ($may_download) {
      $headers = array('Content-Type: ' . mime_header_encode($file->filemime),
                       'Content-Length: ' . $file->filesize);
      file_transfer($file->filepath, $headers);
    }
    else {
      drupal_access_denied();
    }
  }
  else {
    // File not found
    return drupal_not_found();
  }
}


function _upapi_invoke(&$file, $op, $a3 = NULL, $a4 = NULL) {
  foreach (module_implements('upapi') as $name) {
    $function = $name . '_upapi';
    $function($file, $op, $a3, $a4);
  }
  return $a3;
}


/**
 * helper function to keep track of newly created objects.
 */
function _upapi_id_helper($type, $id = NULL) {
  static $ids = array();
  if ($id)
    $ids[$type] = $id;
  
  return $ids[$type];
}

?>